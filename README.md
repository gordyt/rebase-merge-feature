# Rebasing and Merging

## Getting Started

For this tutorial we will be doing everything using git command line
functions.  I will assume that you have git installed.

Checkout a copy of the repository by navigating to the parent
directory where you want this tutorial to reside and clone it as
follows:

    git clone git@gitlab.com:gordyt/rebase-merge-feature.git

Then `cd` into the `rebase-merge-feature` directory.

In this exercise, we start with a repository that looks like this:

    * 9ff4f2a (origin/feature-finish, feature-finish) added poem credits
    * 67ccd39 fixup - swapped verses (had order wrong)
    * 1ded454 added last two verses
    | * 92277a6 (origin/develop, develop) verse 4
    | * f501825 verse 3
    |/
    * e3fe280 (HEAD, origin/master, master) second verse
    * aded5af first verse
    * 907dac6 added README.md

The hexadecimal values are abbreviated commit hashes.  Do no be
concerned if you have different values that what is shown as the
example may have been updated and that will cause those to change.

There will be several parts to this exercise.  For each part, I will
show the git commands that are required to achieve the desired
effect.  To start off, how does one get a listing in that format?

The required command is this:

    git log --graph --decorate --pretty=oneline --abbrev-commit --all

That gets tedious to type all of the time.  You can create an alias
for that command as follows:

Edit the file `$HOME/.gitconfig`, creating it if necessary.  At a
minimum, this file should contain the following section:

    [user]
    	name = <your-first-name> <your-last-name>
    	email = <your-email-address>

We are going to add the following section to the file:

    [alias]
        st = status
        ci = commit
        br = branch
        co = checkout
        cp = cherry-pick
        df = diff
        dc = diff --cached
        # Use this command to generate list of files that are part of a branch; e.g.,
        # git dt <first-commit> <last-commit>
        dt = diff-tree -r
        lg = log -p
        lola = log --graph --decorate --pretty=oneline --abbrev-commit --all
        ls = ls-files

Now the alias in question is the `lola` alias.  You do not need to add
all of the other aliases.  I have included them just to give you some
ideas.

Once you have updated and saved your `$HOME/.gitconfig` file, you can
then issue this command to view the commits as shown above:

    git lola

## First Assignment


- Check out the `feature-finish` branch.
- Merge these two commits into one by doing an interactive 
  rebase against the first commit that comes _before_ the three
  commits that were added by the `feature-finish` branch.  In the
  command example shown, you can use the commit hash or you can use
  any of the labels that are associated with the first commit that is
  _before_ those three commits. You want to keep the `aded last two
  verses` commit message:
    - `fixup - swapped verses (had order wrong)`
    - `added last two verses`


            git checkout feature-finish
            git rebase --interactive --preserve-merges master

Note that when you issue that second command, you will be presented
with a list of the commits involved in an editor window.  The type of
editor used is dependent on the value of your `EDITOR` environment
variable:

    $ echo $EDITOR
    vim

Your mileage, as they say, may vary.  The information presented in the
editor will look something like this:

    pick 1ded454 added last two verses
    pick 67ccd39 fixup - swapped verses (had order wrong)
    pick 9ff4f2a added poem credits

There will be some additional lines, all starting with a `#`, which
means they are comment lines and are ignored by git.  They will
contain helpful option information.  What we want to do is "fixup" the
commit whose comment line helpfully starts with "fixup - ...". (Aren't
I nice?).  To achieve that you want to replace the word "pick" at the
beginning of the line with the work "fixup" or just "f" for short.
The result will like like this:

    pick 1ded454 added last two verses
    f 67ccd39 fixup - swapped verses (had order wrong)
    pick 9ff4f2a added poem credits

Then just save and exit the editor.  You should see output that looks
like this:

    [detached HEAD 54e27f7] added last two verses
     1 file changed, 11 insertions(+)
    Successfully rebased and updated refs/heads/feature-finish.

And your commit tree now looks like this:

$ git lola

    * 6c31286 (HEAD, feature-finish) added poem credits
    * 54e27f7 added last two verses
    | * 9ff4f2a (origin/feature-finish) added poem credits
    | * 67ccd39 fixup - swapped verses (had order wrong)
    | * 1ded454 added last two verses
    |/
    | * 92277a6 (origin/develop, develop) verse 4
    | * f501825 verse 3
    |/
    * e3fe280 (origin/master, master) second verse
    * aded5af first verse
    * 907dac6 added README.md

Notice that our `feature-finish` branch now contains only _two_
commits instead of _three_, because we have combined them.  Also
notice that the branch that is labeled `origin/feature-finish` still
looks the same.  Don't worry about that.  The `origin/feature-finish`
branch and all of the other branch names that start with `origin/`
still reflect the state of the remote repository from which you cloned
this tutorial.  In this tutorial we are working _only_ with your local
branches so you may completely ignore any of the `origin/*` branches.

## Second Assignment

Your next assignment is to rebase your `feature-finish` on top of the
`develop` branch.  Pretend we are wanting to update the `develop`
branch so that your QA team can perform integration testing and they
always do that testing off of the `develop` branch.  Rebasing means to
update your commits so that they will originate from a new commit.  In
this case our `feature-finish` branch had originated from the `master`
branch and we now want to make them originate from the `develop`
branch.  When this happens you can get merge conflicts that occur as a
result of other work that went on independently in the target branched
(`develop`) and your jobs as a developer will be to resolve those
merge conflicts.

Here is the command to initiate the process:

    git rebase --preserve-merges develop

You output will look something like this:

    error: could not apply 54e27f7... added last two verses
    
    When you have resolved this problem, run "git rebase --continue".
    If you prefer to skip this patch, run "git rebase --skip" instead.
    To check out the original branch and stop rebasing, run "git rebase --abort".
    Could not pick 54e27f73b935c6f6eea865a79f59b9295f02223c

Yep, you got a merge conflict!  To see what file or files are
involved, just issue this command:

    $ git status
    
    rebase in progress; onto 92277a6
    You are currently rebasing branch 'feature-finish' on '92277a6'.
      (fix conflicts and then run "git rebase --continue")
      (use "git rebase --skip" to skip this patch)
      (use "git rebase --abort" to check out the original branch)
    
    Unmerged paths:
      (use "git reset HEAD <file>..." to unstage)
      (use "git add <file>..." to mark resolution)
    
    	both modified:   the-tyger.txt
    
    no changes added to commit (use "git add" and/or "git commit -a")

We can see that both branches had, in fact, independently updated
`the-tyger.txt`.  Our job is to edit that file, sort out the issues,
save our changes, stage the file (`git add ...`), and then tell git to
continue with the rebase operation.  Opening `the-tyger.txt` in our
favorite editor (I'll accept either _Vim_ or _Emacs_ as your choice),
we see something like this:

    (1) Tyger Tyger, burning bright, 
        In the forests of the night; 
        What immortal hand or eye, 
        Could frame thy fearful symmetry?
    
    (2) In what distant deeps or skies. 
        Burnt the fire of thine eyes?
        On what wings dare he aspire?
        What the hand, dare seize the fire?
    
    <<<<<<< HEAD
    (3) And what shoulder, & what art,
        Could twist the sinews of thy heart?
        And when thy heart began to beat,
        What dread hand? & what dread feet?
    
    (4) What the hammer? what the chain, 
        In what furnace was thy brain?
        What the anvil? what dread grasp, 
        Dare its deadly terrors clasp! 
    =======
    (5) When the stars threw down their spears 
        And water'd heaven with their tears: 
        Did he smile his work to see?
        Did he who made the Lamb make thee?
    
    (6) Tyger Tyger burning bright, 
        In the forests of the night: 
        What immortal hand or eye,
        Dare frame thy fearful symmetry?
    >>>>>>> 54e27f7... added last two verses

Git helpfully marks the troublesome areas and even tells you where
they came from.  The block of text between these markers...

    <<<<<<< HEAD
    =======

...came from the `HEAD`.  `HEAD` is a special label that is assigned
to the current branch.  The `develop` branch is now the `HEAD` (before
it was `feature-finish` because we had checked it out) because we are
in the process of rebasing our `feature-finish` branch onto `develop`.

The block of text between these two markers...

    =======
    >>>>>>> 54e27f7... added last two verses

...came from our `feature-finish` branch.  In this case, the
hexadecimal value you see is identifying which commit is causing the
merge conflict.  Again YMMV.

I felt sorry for you, so I made this a very simple merge conflict.
You can see by the verse numbers that the information is basically all
there, and already in the correct order.  In this case the only reason
that git raised a red flag was because the patches that are being
applied include some of the surrounding context so the system know where
to apply your changes.  But since that surrounding context was
essentially the same for the work that was done in the `develop`
branch independently of our work in the `feature-finish` branch, git
needs to to help it out so that it doesn't accidentally make an
incorrect assumption.  So all you need to do is edit the file to
look like this:

    (1) Tyger Tyger, burning bright, 
        In the forests of the night; 
        What immortal hand or eye, 
        Could frame thy fearful symmetry?
    
    (2) In what distant deeps or skies. 
        Burnt the fire of thine eyes?
        On what wings dare he aspire?
        What the hand, dare seize the fire?
    
    (3) And what shoulder, & what art,
        Could twist the sinews of thy heart?
        And when thy heart began to beat,
        What dread hand? & what dread feet?
    
    (4) What the hammer? what the chain, 
        In what furnace was thy brain?
        What the anvil? what dread grasp, 
        Dare its deadly terrors clasp! 
    
    (5) When the stars threw down their spears 
        And water'd heaven with their tears: 
        Did he smile his work to see?
        Did he who made the Lamb make thee?
    
    (6) Tyger Tyger burning bright, 
        In the forests of the night: 
        What immortal hand or eye,
        Dare frame thy fearful symmetry?

After you make these simple changes, save the file and issue the
following commands to let git know that everything is OK and it may
proceed with the rebase operation:

git add the-tyger.txt
git rebase --continue

Git will git you an opportunity to alter the commit message by opening
up a buffer in your `$EDITOR`:

    added last two verses
    
    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.
    # rebase in progress; onto 92277a6
    # You are currently rebasing branch 'feature-finish' on '92277a6'.
    #
    # Changes to be committed:
    #       modified:   the-tyger.txt

We don't want to make any changes to the exiting message, so just save
and exit.  You should get back something like this:


    [detached HEAD 1fd21fb] added last two verses
     1 file changed, 9 insertions(+)
     Successfully rebased and updated refs/heads/feature-finish.

Let's see what things look like now:

    $ git lola
    
    * 6b5ccfa (HEAD, feature-finish) added poem credits
    * 1fd21fb added last two verses
    * 92277a6 (origin/develop, develop) verse 4
    * f501825 verse 3
    | * 9ff4f2a (origin/feature-finish) added poem credits
    | * 67ccd39 fixup - swapped verses (had order wrong)
    | * 1ded454 added last two verses
    |/
    * e3fe280 (origin/master, master) second verse
    * aded5af first verse
    * 907dac6 added README.md

I want to point out a couple of things:

- Notice that the `HEAD` has now been assigned back to the
  `feature-finish` branch, as our rebase operation has finished.
- Also notice that our two commits in our `feature-finish` branch that
  used to originate with the `master` branch, now originate, or are
  based off of, the `develop` branch.  Well done!

## Third Assignment

What I want you to do now is to "loop" the two commits that came from
your `feature-finish` branch, effectively grouping them together.  You
will see what I am talking about at the end of this assignment.  I
also want to point out there are other ways to accomplish the previous
assignment and this one in a single step.  I have split them up on
purpose so that I could illustrate this useful technique.

We are doing to check out the `develop` branch and then do a merge
operation on the `feature-finish` branch, instructing git to NOT do
what is called a "fast forward" merge.  A fast forward merge can apply
in any merge situation where the commits that are being merge may be
applied with no modification to the destination.  So, enter the
following commands:

    git checkout develop
    git merge --no-ff feature-finish

Performing a no-fast-forward merge creates an essentially empty "merge
commit" and so you are given the opportunity so update the default
commit message that is associated with the merge commit.  This is what
is pulled in in my $EDITOR:

    Merge branch 'feature-finish' into develop
    
    # Please enter a commit message to explain why this merge is necessary,
    # especially if it merges an updated upstream into a topic branch.
    #
    # Lines starting with '#' will be ignored, and an empty message aborts
    # the commit.

I think that commit message is fine for now, so just save and exit.
Let's see what our commit tree looks like now:

    $ git lola
    
    *   b3936ba (HEAD, develop) Merge branch 'feature-finish' into develop
    |\
    | * 6b5ccfa (feature-finish) added poem credits
    | * 1fd21fb added last two verses
    |/
    * 92277a6 (origin/develop) verse 4
    * f501825 verse 3
    | * 9ff4f2a (origin/feature-finish) added poem credits
    | * 67ccd39 fixup - swapped verses (had order wrong)
    | * 1ded454 added last two verses
    |/
    * e3fe280 (origin/master, master) second verse
    * aded5af first verse
    * 907dac6 added README.md

See how our two commits from the `feature-finish` branch have been
grouped together in a nice little loop?  It makes it obvious to anyone
who comes along later to see exactly what was done for that feature.

Notice also that the `HEAD` still on the `develop` branch that we had
checkout out just prior to performing the merge operation.

## Forth Assignment

Here we want to checkout the `master` branch and merge the
newly-updated `develop` branch into it.  This is done using the exact
same technique that we used in the previous assignment:

    git checkout master
    git merge --no-ff develop

We get another opportunity to update the merge commit message.  Just
leave it at the default value.  Now how do things look?

    *   be1aec9 (HEAD, master) Merge branch 'develop'
    |\
    | *   b3936ba (develop) Merge branch 'feature-finish' into develop
    | |\
    | | * 6b5ccfa (feature-finish) added poem credits
    | | * 1fd21fb added last two verses
    | |/
    | * 92277a6 (origin/develop) verse 4
    | * f501825 verse 3
    |/
    | * 9ff4f2a (origin/feature-finish) added poem credits
    | * 67ccd39 fixup - swapped verses (had order wrong)
    | * 1ded454 added last two verses
    |/
    * e3fe280 (origin/master) second verse
    * aded5af first verse
    * 907dac6 added README.md

That's really cool!  We looped the entire `develop` branch into a
single merge commit.  Notice that we can clearly see all of the work
that was done, but the two independent commits that were made to the
`develop` branch while we were off crafting the `feature-finish`
branch, and the two commits that are still separately "looped" from
the `feature-finish` branch.

Since we are done with the `feature-finish` branch, we can delete it.

    git branch -D feature-finish

And we can now move or promote the `develop` branch up to the `master`
branch commit, making it ready for the next round of fabulous features!

    git branch -f develop HEAD

Let's see our finished work:

    $ git lola
    
    *   be1aec9 (HEAD, master, develop) Merge branch 'develop'
    |\
    | *   b3936ba Merge branch 'feature-finish' into develop
    | |\
    | | * 6b5ccfa added poem credits
    | | * 1fd21fb added last two verses
    | |/
    | * 92277a6 (origin/develop) verse 4
    | * f501825 verse 3
    |/
    | * 9ff4f2a (origin/feature-finish) added poem credits
    | * 67ccd39 fixup - swapped verses (had order wrong)
    | * 1ded454 added last two verses
    |/
    * e3fe280 (origin/master) second verse
    * aded5af first verse
    * 907dac6 added README.md

Perfect!  And if you had write access to the _origin_ repository, you
could "push" your updated branches back to the origin:

    git push origin master
    git push origin develop
    # The following will delete the feature-finish branch on origin.
    # We don't need it any more
    git push origin :feature-finish

## Credits

Full credit to
[William Blake](http://www.poetryfoundation.org/poem/172943) for his
bad-ass poem!


